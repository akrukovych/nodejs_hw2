const successResponse = (response, status = 200, payload = 'success') => {
    payload = typeof payload === 'string' ? { message: payload } : payload;
    response.status(status).json(payload);
};

module.exports = successResponse;
