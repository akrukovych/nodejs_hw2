const jwt = require('jsonwebtoken');
const createError = require('http-errors');
const status = require('http-status');

const {
  tokenNotProvided,
  invalidToken,
  notAuthHeader,
} = require('../messages/messages.constants');

async function verifyToken(req, res, next) {
  const auth = req.header('Authorization');

  if (!auth) return next(createError(status.UNAUTHORIZED, notAuthHeader));

  const [, token] = auth;

  if (!token) return next(createError(status.UNAUTHORIZED, tokenNotProvided));

  try {
    req.user = jwt.verify(token, process.env.TOKEN_SECRET);
    next();
  } catch (e) {
    return next(createError(status.UNAUTHORIZED, invalidToken));
  }
}

module.exports = verifyToken;
