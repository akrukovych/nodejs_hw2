const createError = require('http-errors');
const status = require('http-status');

const User = require('../models/User');
const { userCannotBeFound } = require('../messages/messages.constants');

module.exports = async (req, res, next) => {
    const { _id } = req.user;
    try {
        const user = await User.findById(_id);

        if (user) {
            req.user = user;
            next();
        } else {
            return next(createError(status.FORBIDDEN, userCannotBeFound));
        }
    } catch (error) {
        return next(createError(status.INTERNAL_SERVER_ERROR, error.message));
    }
};
