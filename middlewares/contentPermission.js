const createError = require('http-errors');
const status = require('http-status');

const Note = require('../models/Note');
const { contentNotPermitted } = require('../messages/messages.constants');

async function contentPermission(req, res, next) {
    const {
        user: { _id: userId },
        params: { id: _id },
    } = req;

    const note = await Note.findOne({ _id, userId });

    if (note) {
        req.note = note;
        return next();
    }
    return next(createError(status.FORBIDDEN, contentNotPermitted));
}

module.exports = contentPermission;
