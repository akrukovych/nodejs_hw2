const Joi = require('joi');
const createError = require('http-errors');

const userCredentialsSchema = Joi.object({
    username: Joi.string().max(255).required(),
    password: Joi.string().min(4).max(255).required(),
});

const validateUserCredentials = async (req, res, next) => {
    try {
        await userCredentialsSchema.validateAsync(req.body);
    } catch ({
        details: {
            0: { message },
        },
    }) {
        next(createError(400, message));
    }
    next();
};

module.exports = validateUserCredentials;
