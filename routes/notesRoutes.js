const router = require('express').Router();

const contentPermission = require('../middlewares/contentPermission');
const {
    postNote,
    getNotes,
    getNoteById,
    checkNoteById,
    deleteNoteById,
    updateNoteById,
} = require('../controllers/notesControllers');

router.post('/', postNote);
router.get('/', getNotes);
router.use('/:id', contentPermission);
router.get('/:id', getNoteById);
router.put('/:id', updateNoteById);
router.patch('/:id', checkNoteById);
router.delete('/:id', deleteNoteById);

module.exports = router;
