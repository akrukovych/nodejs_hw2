const router = require('express').Router();

const {
    getUserInfo,
    deleteUser,
    changeUserPassword,
} = require('../controllers/userController');

router.get('/me', getUserInfo);
router.delete('/me', deleteUser);
router.patch('/me', changeUserPassword);

module.exports = router;
