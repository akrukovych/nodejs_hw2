const createError = require('http-errors');
const status = require('http-status');

const Note = require('../models/Note');
const successResponse = require('../responseHandlers/success');
const { contentDeleted } = require('../messages/messages.constants');

const postNote = async (req, res, next) => {
    const {
        user: { _id: userId },
        body: { text },
    } = req;

    if (!text) return next(createError(status.BAD_REQUEST));

    try {
        const note = new Note({ userId, text });
        await note.save();
        return successResponse(res, status.OK, note);
    } catch (error) {
        return next(createError(status.INTERNAL_SERVER_ERROR, error.message));
    }
};

const getNotes = async (req, res, next) => {
    const {
        user: { _id: userId },
    } = req;

    try {
        const notes = await Note.find({ userId });
        return successResponse(res, status.OK, { notes });
    } catch (error) {
        return next(createError(status.INTERNAL_SERVER_ERROR, error.message));
    }
};

const getNoteById = async (req, res, next) => {
    const { note } = req;

    try {
        return successResponse(res, status.OK, note);
    } catch (error) {
        return next(createError(status.INTERNAL_SERVER_ERROR, error.message));
    }
};

const updateNoteById = async (req, res, next) => {
    const {
        note,
        body: { text },
    } = req;

    if (!text) return next(createError(status.BAD_REQUEST));

    try {
        note.text = text;
        await note.save();
        return successResponse(res, status.OK, note);
    } catch (error) {
        return next(createError(status.INTERNAL_SERVER_ERROR, error.message));
    }
};

const checkNoteById = async (req, res, next) => {
    const { note } = req;

    try {
        note.completed = !note.completed;
        await note.save();
        return successResponse(res, status.OK, note);
    } catch (error) {
        return next(createError(status.INTERNAL_SERVER_ERROR, error.message));
    }
};

const deleteNoteById = async (req, res, next) => {
    const { note } = req;

    try {
        await note.delete();
        return successResponse(res, status.OK, contentDeleted);
    } catch (error) {
        return next(createError(status.INTERNAL_SERVER_ERROR, error.message));
    }
};

module.exports.postNote = postNote;
module.exports.getNotes = getNotes;
module.exports.getNoteById = getNoteById;
module.exports.updateNoteById = updateNoteById;
module.exports.checkNoteById = checkNoteById;
module.exports.deleteNoteById = deleteNoteById;
