const bcrypt = require('bcryptjs');
const createError = require('http-errors');
const status = require('http-status');

const successResponse = require('../responseHandlers/success');

const {
  credentialsNotProvided,
  passwordNotMatch,
} = require('../messages/messages.constants');
const { isEqualPasswords } = require('../utils');

const getUserInfo = async (req, res, next) => {
  const { user } = req;
  try {
    return successResponse(res, status.OK, user);
  } catch (error) {
    return next(createError(status.INTERNAL_SERVER_ERROR, error.message));
  }
};

const deleteUser = async (req, res, next) => {
  const user = req.user;
  try {
    await user.delete();
    return successResponse(res, status.OK);
  } catch (error) {
    return next(createError(status.INTERNAL_SERVER_ERROR, error.message));
  }
};

const changeUserPassword = async (req, res, next) => {
  const {
    user,
    body: { oldPassword, newPassword },
  } = req;

  try {
    if (!oldPassword || !newPassword) {
      return next(createError(status.BAD_REQUEST, credentialsNotProvided));
    }

    if (await isEqualPasswords(oldPassword, user.password)) {
      user.password = newPassword;

      try {
        await user.save();
        return successResponse(res);
      } catch (error) {
        return next(createError(status.INTERNAL_SERVER_ERROR, error.message));
      }
    } else {
      return next(createError(status.BAD_REQUEST, passwordNotMatch));
    }
  } catch (error) {
    return next(createError(status.INTERNAL_SERVER_ERROR, error.message));
  }
};

module.exports.getUserInfo = getUserInfo;
module.exports.deleteUser = deleteUser;
module.exports.changeUserPassword = changeUserPassword;
