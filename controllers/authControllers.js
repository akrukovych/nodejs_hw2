const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const createError = require('http-errors');
const status = require('http-status');

const User = require('../models/User');
const successResponse = require('../responseHandlers/success');
const {
  invalidPassword,
  userAlreadyExists,
  userNotExists,
} = require('../messages/messages.constants');
const { isEqualPasswords } = require('../utils');

const register = async (request, response, next) => {
  let user;
  const { username, password } = request.body;

  user = await User.findOne({ username });

  if (user) return next(createError(status.FORBIDDEN, userAlreadyExists));

  try {
    user = new User({ username, password });
    await user.save();
    return successResponse(response, status.CREATED);
  } catch (error) {
    return next(createError(status.INTERNAL_SERVER_ERROR, error.message));
  }
};

const login = async (request, response, next) => {
  const { username, password } = request.body;

  const user = await User.findOne({ username });

  if (user) {
    if (await isEqualPasswords(password, user.password)) {
      const jwt_token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
      return successResponse(response, status.OK, { jwt_token });
    }
    return next(createError(status.BAD_REQUEST, invalidPassword));
  }

  return next(createError(status.BAD_REQUEST, userNotExists));
};

module.exports.register = register;
module.exports.login = login;
