const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
require('dotenv').config();

const authRouter = require('./routes/authRoutes');
const userRouter = require('./routes/userRoutes');
const notesRouter = require('./routes/notesRoutes');

const validateUserCredentials = require('./middlewares/validateUserCredentials');
const verifyToken = require('./middlewares/verifyToken');
const errorHandler = require('./middlewares/errorHandler');
const getUser = require('./middlewares/getUser');

const app = express();

app.use(express.json());
app.use(morgan('dev'));

app.use('/api/auth', validateUserCredentials, authRouter);
app.use('/api/users', verifyToken, getUser, userRouter);
app.use('/api/notes', verifyToken, notesRouter);
app.use(errorHandler);

async function run() {
  try {
    await mongoose.connect(
      process.env.DB_URI || 'mongodb://localhost/node_hw2',
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      }
    );
    app.listen(process.env.PORT || 3000, () => {
      console.log('server starts');
    });
  } catch (e) {
    console.error(e);
  }
}

run();
