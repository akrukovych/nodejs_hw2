const faker = require('faker');
const mongoose = require('mongoose');

const User = require('./models/User');
const Note = require('./models/Note');

async function populateDB() {
  await mongoose.connect(
    'mongodb://localhost/node_hw2',
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    },
    async () => {
      await fillUsers();
    }
  );
}

async function fillUsers() {
  const userIds = [];
  for (let i = 0; i < 10; i++) {
    const user = new User({
      username: faker.internet.userName(),
      password: '1111',
    });
    userIds.push(user._id);
    await user.save();
  }
  await fillNotes(userIds);
  await close();
}

async function fillNotes(userIds) {
  for (let i = 0; i < userIds.length; i++) {
    const note = new Note({
      userId: userIds[i],
      text: faker.lorem.text(),
    });
    await note.save();
  }
  await close();
}
async function close() {
  await mongoose.disconnect();
  process.exit();
}

populateDB();
