const mongoose = require('mongoose');

const { hashPassword } = require('../utils');
const Note = require('./Note');

const userSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      unique: true,
      required: true,
      max: 255,
    },
    password: {
      type: String,
      unique: true,
      required: true,
      max: 1024,
    },
    createdDate: { type: Date, default: Date.now },
  },
  { versionKey: false }
);

userSchema.pre('save', async function (next) {
  if (!this.isModified('password')) return next();
  try {
    this.password = await hashPassword(this.password);
    return next();
  } catch (err) {
    return next(err);
  }
});

userSchema.post('delete', async function (next) {
  try {
    await Note.deleteMany({ userId: this._id });
  } catch (err) {
    return next(err);
  }
});

module.exports = mongoose.model('user', userSchema);
