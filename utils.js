const bcrypt = require('bcryptjs');

const hashPassword = async (password) => {
  const salt = await bcrypt.genSalt(+process.env.SALT_WORK_FACTOR);
  return await bcrypt.hash(password, salt);
};

const isEqualPasswords = async (password, hashedPassword) =>
  await bcrypt.compare(password, hashedPassword);

module.exports.hashPassword = hashPassword;
module.exports.isEqualPasswords = isEqualPasswords;
